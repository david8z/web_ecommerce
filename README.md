# web_ecommerce

Code related with e-commerce website of 37 outlet.
Technologies:
- Back-end (Django)
- Back-end Web Server (Gunicorn)
- Front-end (Vue.js)
- Load Balancer (NGINX)
- Container (Docker)
- Orchestrator (Docker-Compose)

Main code under branch v1
